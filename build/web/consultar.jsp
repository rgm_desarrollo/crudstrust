<%-- 
    Document   : consultar
    Created on : 12-jun-2020, 0:08:52
    Author     : jroma
--%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <a href="/registrar"><button class="button-report" type="button">Report</button></a>
        	<s:if test="noData==true">
        <table>
				<thead>
					<tr style="background-color: #E0E0E1;">
						<th>id</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Color</th>
						<th>Propietario</th>						
					</tr>
				</thead>
				<s:iterator value="Lista">
					<tr>
						<td><s:property value="id" /></td>
						<td><s:property value="Marca" /></td>
						<td><s:property value="Modelo" /></td>
						<td><s:property value="Color" /></td>
						<td><s:property value="Propietario" /></td>
						
					</tr>
				</s:iterator>
        </table>
                </s:if>
                <s:else>
                    <div style="color: red;">No Data Found.</div>
                </s:else>
        
        
    </body>
</html>
