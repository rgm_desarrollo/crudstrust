<%-- 
    Document   : registrar
    Created on : 12-jun-2020, 0:08:43
    Author     : jroma
--%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>REGISTRO DE COMPUTADORAS</title>
        <style type="text/css">
        .button-register {background-color: green;color: white;}
        .button-report {background-color: #000000;color: white;margin-left: 30%;}
        </style>
    </head>
    <body>   
        
        <s:form action="ComputadoraActions"  method="post">
		<s:textfield label="Marca" name="compu.Marca" />
		<s:textfield label="Modelo" name="compu.Modelo" />
		<s:textfield label="Color" name="compu.Color" />
		<s:textfield label="Propietario" name="compu.Propietario" />
                <s:textfield label="numero_propietario" name="compu.numero_propietario" />                
		<s:submit cssClass="button-register" value="Guardar" />
	</s:form>
	<s:if test="ctr>0">
		<span style="color: green;"><s:property value="msg" /></span>
	</s:if>
	<s:else>
		<span style="color: red;"><s:property value="msg" /></span>
	</s:else>
        
        <a href="ComputadoraListarActions"><button class="button-report" type="button">Report</button></a>
        
    </body>
</html>
