/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Computadora;
import modelo.Conexion;

/**
 *
 * @author jroma
 */
public class DaoComputadora {

   
   /**
    * Metodo para crear computadora
    * @param parametro verdadero o falso
    * @return
    * @throws Exception 
    */
   public boolean CrearComputadora(Computadora compu) throws Exception
   {
                Conexion c = new Conexion();
                c.Conexion();         
                       
		boolean res=false;
		try {
                            String sql = "INSERT INTO computadora(Marca,Modelo,Color,Propietario,numero_propietario) VALUES (?,?,?,?,?)";
			PreparedStatement ps = c.getCon().prepareStatement(sql);
			ps.setString(1, compu.getMarca());
			ps.setString(2, compu.getModelo());
			ps.setString(3, compu.getColor());
			ps.setString(4, compu.getPropietario());
                        ps.setString(5, compu.getNumero_propietario());
			
                        ps.execute();
                        ps.close();
                        
                        
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (c.con!= null) {
				c.con.close();
			}
		}
	}
   /**
    * Metodo para listar los datos de la computadora
    * @return
    * @throws SQLException
    * @throws Exception 
    */
   	public ResultSet Listar() throws SQLException, Exception {
            Conexion c = new Conexion(); 
              c.Conexion();
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM COMPUTADORA";
			PreparedStatement ps = c.getCon().prepareStatement(sql);
			rs = ps.executeQuery();
			
		} catch (Exception e) {
			e.printStackTrace();
                        System.out.println(e.getMessage());
                        
			
		}
               
                return rs;
	}
   
   
    
    
}
