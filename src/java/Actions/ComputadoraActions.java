/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Actions;

import Dao.DaoComputadora;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.PreparedStatement;
import java.util.Objects;
import modelo.Computadora;
import modelo.Conexion;

/**
 *
 * @author jroma
 */
public class ComputadoraActions extends ActionSupport
{
      private static final long serialVersionUID = 2139116285823340125L;        
      private Conexion c = new Conexion();
      private Computadora  compu = new Computadora();
      
   @Override
    public String execute()  {
       {
           try {
               DaoComputadora dao = new DaoComputadora();
               dao.CrearComputadora(compu);
                this.msg="Registro Guardado Correctamente";

           } catch (Exception ex) {
                 this.msg = "Error al registrar la computadora" + ex.getMessage();

           }

       }
             
           
            return "ok";

        
    }
      
      

    public Computadora getCompu() {
        return compu;
    }

    public void setCompu(Computadora compu) {
        this.compu = compu;
    }
      private String msg = ""; 
     private int crt=0;

    
     
   
    public int getCrt() {
        return crt;
    }

    public void setCrt(int crt) {
        this.crt = crt;
    }
    
 
    

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
  
}
