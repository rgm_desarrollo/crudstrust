/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Actions;

import Dao.DaoComputadora;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.Computadora;
import modelo.Conexion;

/**
 *
 * @author jroma
 */
public class ComputadoraListarActions extends ActionSupport 
{

    private static final long serialVersionUID = 6329394260276112660L;
    ResultSet rs = null;
    Computadora compu = null;
    public List<Computadora> lista = null;
    
    private boolean noData = false;

    public boolean isNoData() {
        return noData;
    }

    public void setNoData(boolean noData) {
        this.noData = noData;
    }

    public String execute() throws Exception {
        try {
            Conexion c = new Conexion();
            c.Conexion();
            DaoComputadora dao = new DaoComputadora();
            int i = 0;
            lista = new ArrayList<Computadora>();
            rs = dao.Listar();
            System.out.println("listando computadoras");
            if (rs != null) {
                while (rs.next()) {
                    i++;
                    compu = new Computadora();
                    compu.setId(rs.getInt("id"));
                    compu.setMarca(rs.getString("Marca"));
                    compu.setModelo(rs.getString("Modelo"));
                    compu.setColor(rs.getString("Color"));
                    compu.setPropietario(rs.getString("Propietario"));
                    compu.setNumero_propietario(rs.getString("numero_propietario"));
                    lista.add(compu);
                    System.out.println(lista);
                }

            }
            if (i == 0) {
                noData = false;
            } else {
                noData = true;
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "REPORT";
    }

    public List<Computadora> getLista() {
        return lista;
    }

    public void setLista(List<Computadora> lista) {
        this.lista = lista;
    }

}
