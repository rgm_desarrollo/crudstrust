/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author jroma
 */
public class Computadora {

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the Marca
     */
    public String getMarca() {
        return Marca;
    }

    /**
     * @param Marca the Marca to set
     */
    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    /**
     * @return the Modelo
     */
    public String getModelo() {
        return Modelo;
    }

    /**
     * @param Modelo the Modelo to set
     */
    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    /**
     * @return the Color
     */
    public String getColor() {
        return Color;
    }

    /**
     * @param Color the Color to set
     */
    public void setColor(String Color) {
        this.Color = Color;
    }

    /**
     * @return the Propietario
     */
    public String getPropietario() {
        return Propietario;
    }

    /**
     * @param Propietario the Propietario to set
     */
    public void setPropietario(String Propietario) {
        this.Propietario = Propietario;
    }

    /**
     * @return the numero_propietario
     */
    public String getNumero_propietario() {
        return numero_propietario;
    }

    /**
     * @param numero_propietario the numero_propietario to set
     */
    public void setNumero_propietario(String numero_propietario) {
        this.numero_propietario = numero_propietario;
    }
    private  int id;
    private String Marca;
    private String Modelo;
    private String Color;
    private String Propietario;
    private String numero_propietario;
    
    
    
}
